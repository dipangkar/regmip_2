src <- "Y:\\GRP-HGIS\\Public\\GCM\\Processed\\GCM2\\"
dest <- "Y:\\GRP-HGIS\\Public\\GCM\\SWATinput\\GCM2\\"

load("C:/WorkPlace/R_Data/GCM2SWAT.RData")
save.image("C:/WorkPlace/R_Data/GCM2SWAT.RData")

GCM2SWAT <- function(src, dest){
      setwd(src)
      fname <- list.files(pattern = ".csv")
      # type of rcp and historical:
      rcp.typ <- c("historical", "rcp2p6", "rcp4p5", "rcp6p0","rcp8p5")
      # subsetting clim data type:
      index.pr <- grep("pr",fname, fixed = T)
      index.rhs <- grep("rhs", fname,fixed = T)
      index.rsds <- grep("rsds",fname,fixed = T)
      index.tmax <- grep("tasmax",fname, fixed = T)
      index.tmin <- grep("tasmin",fname, fixed = T)
      index.wind <- grep("wind", fname, fixed = T)
      # Now working thorugh each of the rcp's and historical data
      # for pcp
      for(i in 1:length(rcp.typ)){
            dir.create(paste(dest,rcp.typ[i], sep = ""))
            filelist <- fname[index.pr] 
            filename <- filelist[grep(rcp.typ[i],filelist , fixed = TRUE)]
            process.fun(filename,var.typ = "pcp", dest = paste(dest,rcp.typ[i],"\\", sep = ""), s.name = "p")
            # for rhs
            filelist <- fname[index.rhs]
            filename.rhs <- filelist[grep(rcp.typ[i], filelist, fixed = TRUE)]
            process.fun(filename.rhs,var.typ = "hmd", dest = paste(dest,rcp.typ[i],"\\", sep = ""), s.name = "h")
            # for solar radiation
            filelist <- fname[index.rsds]
            filename.rsds <- filelist[grep(rcp.typ[i], filelist, fixed = TRUE)]
            process.fun(filename.rsds,var.typ = "slr", dest = paste(dest,rcp.typ[i],"\\", sep = ""), s.name = "s")
            # for wind speed
            filelist <- fname[index.wind]
            filename.wind <- filelist[grep(rcp.typ[i], filelist, fixed = TRUE)]
            process.fun(filename.wind,var.typ = "wnd", dest = paste(dest,rcp.typ[i],"\\", sep = ""), s.name = "w")
            # for temperature
            filelist_max <- fname[index.tmax]
            filelist_min <- fname[index.tmin]
            filename_max <- filelist_max[grep(rcp.typ[i], filelist_max, fixed = TRUE)]
            filename_min <- filelist_min[grep(rcp.typ[i], filelist_min, fixed = TRUE)]
            process.tmp.fun(filename_max,filename_min, var.typ = "tmp", 
                            dest = paste(dest,rcp.typ[i],"\\", sep = ""), s.name = "t")
      }
      print ("Process finished......")
}



GCM2SWAT(src,dest)


## For processing the station information data

station.info(st.file = Louth_elev, var.typ = clim.typ)
